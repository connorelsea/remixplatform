import styled from "styled-components/native"

export default styled.View`
  background-color: white;
  padding: 12px;
  border-radius: 8px;
  shadow-color: black;
  shadow-radius: 22px;
  shadow-offset: 0px 10px;
  shadow-opacity: 0.09;
  width: 100%;
`
