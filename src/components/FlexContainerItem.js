import styled from "styled-components/native"

export default styled.View`
  align-items: flex-start;
  width: 45%;

  @media (max-width: 999px) {
    width: 100%;
  }
`
