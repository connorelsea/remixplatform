import styled from "styled-components/native"

export default styled.View`
  flex-direction: column;
  justify-content: space-between;
`