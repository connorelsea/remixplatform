export default {
  colors: {
    grey: {
      // 100: "#F8F8F8",
      100: "#F1F0F7",
      200: "#D1D5DB",
      300: "#BBC2CA",
      400: "#ADB3BD",
      500: "#7A7F88",
      600: "#1A1B1D",
    },
    blue: {
      400: "#007AFF",
    },
  },
}
